package com.jkorpics.collections;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.*;

public class MappedTreeSetTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test_add_contains() {
        MappedTreeSet<LastUserLogin> set = new MappedTreeSet<>();
        LastUserLogin login =
                new LastUserLogin(
                        "user1",
                        LocalDateTime.of(2019, 4, 16, 10, 15, 0)
                );
        assertFalse(set.contains(login));
        set.add(login);
        assertTrue(set.contains(login));
    }

    @Test
    public void test_add_overwriteEqualEntries() {
        MappedTreeSet<LastUserLogin> set = new MappedTreeSet<>();
        String user = "user1";
        LocalDateTime baseTime = LocalDateTime.of(2019, 4, 16, 10, 15, 0);
        LastUserLogin firstLogin = new LastUserLogin(user, baseTime);
        LastUserLogin secondLogin = new LastUserLogin(user, baseTime.plusHours(2));
        assertTrue(set.add(firstLogin));
        assertTrue(set.add(secondLogin));
        assertTrue(set.contains(secondLogin));
        assertEquals(1, set.size());
        for (LastUserLogin l : set) {
            assertEquals(secondLogin.loginTIme, l.loginTIme);
            assertNotEquals(firstLogin.loginTIme, l.loginTIme);
        }
    }

    @Test
    public void test_add_iterationOrder() {
        MappedTreeSet<LastUserLogin> set = new MappedTreeSet<>();
        List<LastUserLogin> ordererdLogins = getOrderedLogins(5);
        addInReverse(set, ordererdLogins);
        assertEquals(ordererdLogins.size(), set.size());
        int i = 0;
        for (LastUserLogin l : set) {
            LastUserLogin expected = ordererdLogins.get(i++);
            assertEquals(expected.user, l.user);
            assertEquals(expected.loginTIme, l.loginTIme);
        }
    }

    @Test
    public void test_remove_basedOnEquals() {
        MappedTreeSet<LastUserLogin> set = new MappedTreeSet<>();
        String user = "user1";
        LocalDateTime baseTime = LocalDateTime.of(2019, 4, 16, 10, 15, 0);
        LastUserLogin firstLogin = new LastUserLogin(user, baseTime);
        LastUserLogin secondLogin = new LastUserLogin(user, baseTime.plusHours(2));
        assertTrue(set.add(firstLogin));
        assertTrue(set.remove(secondLogin));
        assertFalse(set.contains(secondLogin));
        assertEquals(0, set.size());
    }

    @Test
    public void test_clear() {
        MappedTreeSet<LastUserLogin> set = new MappedTreeSet<>();
        List<LastUserLogin> ordererdLogins = getOrderedLogins(5);
        addInReverse(set, ordererdLogins);
        assertEquals(5, set.size());
        set.clear();
        assertEquals(0, set.size());
        for (LastUserLogin l : ordererdLogins) {
            assertFalse(set.contains(l));
        }
        for (LastUserLogin l : set) {
            fail("There should not be any entries in a cleared set.");
        }
    }

    @Test
    public void test_addAll() {
        MappedTreeSet<LastUserLogin> set = new MappedTreeSet<>();
        List<LastUserLogin> ordererdLogins = getOrderedLogins(5);
        set.addAll(ordererdLogins);
        assertEquals(5, set.size());
        for (LastUserLogin l : ordererdLogins) {
            assertTrue(set.contains(l));
        }
    }

    private List<LastUserLogin> getOrderedLogins(final int numLogins) {
        List<LastUserLogin> orderedLogins = new ArrayList<>();
        LocalDateTime baseTime =
                LocalDateTime.of(2019, 4, 16, 7, 15, 0);
        for (int i = 0; i < numLogins; i++) {
            orderedLogins.add(
                    new LastUserLogin(
                            "user" + i,
                            baseTime.plusHours(i)
                    )
            );
        }
        return orderedLogins;
    }

    private void addInReverse(final MappedTreeSet<LastUserLogin> set,
                              final List<LastUserLogin> logins) {
        // first add all the logins in reverse login order
        for (int i = logins.size() - 1; i >= 0; i--) {
            set.add(logins.get(i));
        }
    }

    private static class LastUserLogin implements Comparable<LastUserLogin> {
        String user;
        LocalDateTime loginTIme;

        public LastUserLogin(String user, LocalDateTime loginTIme) {
            this.user = user;
            this.loginTIme = loginTIme;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            LastUserLogin that = (LastUserLogin) o;
            return user.equals(that.user);
        }

        @Override
        public int hashCode() {
            return Objects.hash(user);
        }

        @Override
        // comparator different from equals
        public int compareTo(LastUserLogin o) {
            return this.loginTIme.compareTo(o.loginTIme);
        }
    }
}

