package com.jkorpics.collections;


/**
 * A hash map whose iteration is based on the sort order of its values.
 * If values are equal, the sort order of the corresponding keys are used.
 *
 * Updating the value of a key will result in re-ordering based on that keys' new value.
 */
public class ValueSortedHashMap {
    // tbd
}
