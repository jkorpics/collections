package com.jkorpics.collections;

import java.util.*;

/**
 * A TreeSet decoration for use where the Comparator is not consistent with the object's equals method.
 * Internally, this class uses a HashMap to track uniqueness, while allowing the TreeSet implementation
 * to perform sorted storage and iteration. Like TreeSet, this class is NOT threadsafe.
 * Whenever an element of the set is re-inserted, the element is re-sorted within the TreeMap. A reinserted
 * element e is one where contains(e) returns true prior to add(e) being called.
 * @param <E>
 */
public class MappedTreeSet<E> extends TreeSet<E>{
    private final Map<E, E> map = new HashMap();

    public MappedTreeSet() {
        super();
    }

    public MappedTreeSet(final Comparator<E> comparator) {
        super(comparator);
    }

    @Override
    public boolean contains(Object obj) {
        return map.get(obj) != null;
    }

    @Override
    public boolean add(E obj) {
        this.remove(obj);
        this.map.put(obj, obj);
        return super.add(obj);
    }

    @Override
    public boolean remove(Object obj) {
        E removed = map.remove(obj);
        return removed == null || super.remove(removed);
    }

    @Override
    public void clear() {
        map.clear();
        super.clear();
    }

    public  boolean addAll(Collection<? extends E> c) {
        boolean r = true;
        for(E item : c) {
            r = this.add(item) && r;
        }
        return r;
    }
}

